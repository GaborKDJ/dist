const pkg = require('./package')


module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
 css: [
  '@/assets/css/main.scss',
],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],



  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    ['@nuxtjs/redirect-module', {
      // Redirect option here
    }],
    // Doc: https://buefy.github.io/#/documentation
    ['nuxt-buefy', { css: true, materialDesignIcons: true}],
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
   extend (config, { isDev, isClient }) {
    config.node = { fs: 'empty', 
                    dgram: 'empty',
                    net: 'empty',
                    tls: 'empty',
                    dns: 'empty'
       },
       config.resolve.alias['vue'] = 'vue/dist/vue.common'
    
  }
}
}
